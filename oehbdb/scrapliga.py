import datetime
import json
import logging
from typing import TypedDict, List
from oehbdb import data

logger = logging.getLogger()


class FeedData(TypedDict):
    championships: List[dict]
    clubs: List[dict]
    groups: List[dict]
    teams: List[dict]
    courts: List[dict]
    matches: List[dict]


def read_feeds(feed_dir: str) -> FeedData:
    feed_data: FeedData = {
        'championships': [],
        'clubs': [],
        'groups': [],
        'teams': [],
        'courts': [],
        'matches': [],
    }

    with open("%s/championships.json" % feed_dir, 'r') as f:
        scrapliga_items = json.load(f)
        for item in scrapliga_items:
            feed_data['championships'].append(item)

    with open("%s/clubs.json" % feed_dir, 'r') as f:
        scrapliga_clubs = json.load(f)
        for item in scrapliga_clubs:
            feed_data['clubs'].append(item)

    with open("%s/groups.json" % feed_dir, 'r') as f:
        scrapliga_data = json.load(f)
        for item in scrapliga_data:
            feed_data['groups'].append(item)

    with open("%s/teams.json" % feed_dir, 'r') as f:
        scrapliga_data = json.load(f)
        for item in scrapliga_data:
            feed_data['teams'].append(item)

    with open("%s/courts.json" % feed_dir, 'r') as f:
        scrapliga_data = json.load(f)
        for item in scrapliga_data:
            feed_data['courts'].append(item)

    with open("%s/matches.json" % feed_dir, 'r') as f:
        scrapliga_data = json.load(f)
        for item in scrapliga_data:
            feed_data['matches'].append(item)

    return feed_data


def update_object(obj: data.NuligaObject, scrapliga_data: dict) -> bool:
    if scrapliga_data['timestamp'] < obj.nuliga_data('nuliga_timestamp'):
        logger.debug("Not updating object %s %s (%s): nuliga data is too old" % (obj.__class__.__name__, obj.name, obj.id))
        return False
    if scrapliga_data['timestamp'] == obj.nuliga_data('nuliga_timestamp'):
        logger.debug("Not updating object %s %s (%s): nuliga data has same timestamp" % (obj.__class__.__name__, obj.name, obj.id))
        return False
    dirty = False
    if obj.nuliga_data('nuliga_id') != scrapliga_data['nuliga_id'] and scrapliga_data['nuliga_id']:
        obj.nuliga_data_set('nuliga_id', scrapliga_data['nuliga_id'])
        dirty = True
    if isinstance(obj, data.Club):
        if obj.nuliga_data('name') != scrapliga_data['name'] and scrapliga_data['name']:
            obj.nuliga_data_set('name', scrapliga_data['name'])
            dirty = True
        if obj.nuliga_data('website') != scrapliga_data['website'] and scrapliga_data['website']:
            obj.nuliga_data_set('website', scrapliga_data['website'])
            dirty = True
    elif isinstance(obj, data.Championship):
        if obj.nuliga_data('name') != scrapliga_data['name'] and scrapliga_data['name']:
            obj.nuliga_data_set('name', scrapliga_data['name'])
            dirty = True
        if obj.nuliga_data('short_name') != scrapliga_data['short_name'] and scrapliga_data['short_name']:
            obj.nuliga_data_set('short_name', scrapliga_data['short_name'])
            dirty = True
    elif isinstance(obj, data.Group):
        if obj.nuliga_data('name') != scrapliga_data['name'] and scrapliga_data['name']:
            obj.nuliga_data_set('name', scrapliga_data['name'])
            dirty = True
        if obj.nuliga_data('championship') != scrapliga_data['championship'] and scrapliga_data['championship']:
            obj.nuliga_data_set('championship', scrapliga_data['championship'])
            dirty = True
        if obj.nuliga_data('standings') != scrapliga_data['standings'] and scrapliga_data['standings']:
            obj.nuliga_data_set('standings', scrapliga_data['standings'])
            dirty = True
    elif isinstance(obj, data.Court):
        if obj.nuliga_data('name') != scrapliga_data['name'] and scrapliga_data['name']:
            obj.nuliga_data_set('name', scrapliga_data['name'])
            dirty = True
        if obj.nuliga_data('address') != scrapliga_data['address'] and scrapliga_data['address']:
            obj.nuliga_data_set('address', scrapliga_data['address'])
            dirty = True
        if obj.nuliga_data('postal_code') != scrapliga_data['postal_code'] and scrapliga_data['postal_code']:
            obj.nuliga_data_set('postal_code', scrapliga_data['postal_code'])
            dirty = True
        if obj.nuliga_data('city') != scrapliga_data['city'] and scrapliga_data['city']:
            obj.nuliga_data_set('city', scrapliga_data['city'])
            dirty = True
    elif isinstance(obj, data.Team):
        if obj.nuliga_data('name') != scrapliga_data['name'] and scrapliga_data['name']:
            obj.nuliga_data_set('name', scrapliga_data['name'])
            dirty = True
        if obj.nuliga_data('match_display_name') != scrapliga_data['display_name'] and scrapliga_data['display_name']:
            obj.nuliga_data_set('match_display_name', scrapliga_data['display_name'])
            dirty = True
        if obj.nuliga_data('club') != scrapliga_data['club_name'] and scrapliga_data['club_name']:
            obj.nuliga_data_set('club', scrapliga_data['club_name'])
            dirty = True
        if obj.nuliga_data('championship') != scrapliga_data['championship'] and scrapliga_data['championship']:
            obj.nuliga_data_set('championship', scrapliga_data['championship'])
            dirty = True
        if obj.nuliga_data('group') != scrapliga_data['group'] and scrapliga_data['group']:
            obj.nuliga_data_set('group', scrapliga_data['group'])
            dirty = True
        if obj.nuliga_data('noncompetitive') != scrapliga_data['noncompetitive'] and scrapliga_data['noncompetitive']:
            obj.nuliga_data_set('noncompetitive', scrapliga_data['noncompetitive'])
            dirty = True
    elif isinstance(obj, data.Match):
        if obj.nuliga_data('championship') != scrapliga_data['championship'] and scrapliga_data['championship']:
            obj.nuliga_data_set('championship', scrapliga_data['championship'])
            dirty = True
        if obj.nuliga_data('group') != scrapliga_data['group'] and scrapliga_data['group']:
            obj.nuliga_data_set('group', scrapliga_data['group'])
            dirty = True
        if obj.nuliga_data('number') != scrapliga_data['match_number'] and scrapliga_data['match_number']:
            obj.nuliga_data_set('number', scrapliga_data['match_number'])
            dirty = True
        if obj.nuliga_data('team_a') != scrapliga_data['team_a'] and scrapliga_data['team_a']:
            obj.nuliga_data_set('team_a', scrapliga_data['team_a'])
            dirty = True
        if obj.nuliga_data('team_b') != scrapliga_data['team_b'] and scrapliga_data['team_b']:
            obj.nuliga_data_set('team_b', scrapliga_data['team_b'])
            dirty = True
        if obj.nuliga_data('date') != scrapliga_data['date'] and scrapliga_data['date']:
            obj.nuliga_data_set('date', scrapliga_data['date'])
            dirty = True
        if obj.nuliga_data('court') != scrapliga_data['court'] and scrapliga_data['court']:
            obj.nuliga_data_set('court', scrapliga_data['court'])
            dirty = True
        if obj.nuliga_data('goals_a') != scrapliga_data['goals_a'] and scrapliga_data['goals_a']:
            obj.nuliga_data_set('goals_a', scrapliga_data['goals_a'])
            dirty = True
        if obj.nuliga_data('goals_b') != scrapliga_data['goals_b'] and scrapliga_data['goals_b']:
            obj.nuliga_data_set('goals_b', scrapliga_data['goals_b'])
            dirty = True
    if dirty:
        obj.nuliga_data_set('nuliga_timestamp', scrapliga_data['timestamp'])
        return True
    else:
        return False


def create_championship_object(scrapliga_data: dict) -> data.Championship:
    return data.Championship(nuliga_data={
        'nuliga_timestamp': scrapliga_data['timestamp'],
        'nuliga_id': scrapliga_data['nuliga_id'],
        'name': scrapliga_data['name'],
        'short_name': scrapliga_data['short_name']
    })


def create_club_object(scrapliga_club_data: dict) -> data.Club:
    return data.Club(nuliga_data={
        'nuliga_timestamp': scrapliga_club_data['timestamp'],
        'nuliga_id': scrapliga_club_data['nuliga_id'],
        'name': scrapliga_club_data['name'],
        'website': scrapliga_club_data['website']
    })


def create_group_object(scrapliga_data: dict) -> data.Group:
    return data.Group(nuliga_data={
        'nuliga_timestamp': scrapliga_data['timestamp'],
        'nuliga_id': scrapliga_data['nuliga_id'],
        'name': scrapliga_data['name'],
        'championship': scrapliga_data['championship'],
        'standings': scrapliga_data['standings']
    })


def create_court_object(scrapliga_data: dict) -> data.Court:
    return data.Court(nuliga_data={
        'nuliga_timestamp': scrapliga_data['timestamp'],
        'nuliga_id': scrapliga_data['nuliga_id'],
        'name': scrapliga_data['name'],
        'address': scrapliga_data['address'],
        'postal_code': scrapliga_data['postal_code'],
        'city': scrapliga_data['city'],
    })


def create_team_object(scrapliga_data: dict) -> data.Team:
    return data.Team(nuliga_data={
        'nuliga_timestamp': scrapliga_data['timestamp'],
        'nuliga_id': scrapliga_data['nuliga_id'],
        'name': scrapliga_data['name'],
        'match_display_name': scrapliga_data['display_name'],
        'championship': scrapliga_data['championship'],
        'club': scrapliga_data['club_name'],
        'group': scrapliga_data['group'],
        'noncompetitive': scrapliga_data['noncompetitive']
    })


def create_match_object(scrapliga_data: dict) -> data.Match:
    return data.Match(nuliga_data={
        'nuliga_timestamp': scrapliga_data['timestamp'],
        'nuliga_id': scrapliga_data['nuliga_id'],
        'championship': scrapliga_data['championship'],
        'group': scrapliga_data['group'],
        'number': scrapliga_data['match_number'],
        'team_a': scrapliga_data['team_a'],
        'team_b': scrapliga_data['team_b'],
        'date': scrapliga_data['date'],
        'court': scrapliga_data['court'],
        'goals_a': scrapliga_data['goals_a'],
        'goals_b': scrapliga_data['goals_b'],
    })



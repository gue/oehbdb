from __future__ import annotations
import copy
import logging
import random
import time
from datetime import datetime

logger = logging.getLogger()

_id_set = set()


def generate_data_id():
    alphabet = "123456789abcdfghjklmnpqrstvwxyzABCDFGHJKLMNPQRSTVWXYZ"
    round = 0
    while True:
        id_candidate = ''.join(random.choice(alphabet) for i in range(8))
        if id_candidate not in _id_set:  # check for id collisions
            return id_candidate
        if round > 10000:
            raise RuntimeError("Generating data id failed: reached 10000 collisions")
        round += 1


class OehbdbObject:
    """
    :param **kwargs:
        See below

    :Keyword Arguments:
        * *created* (``int``) --
          Unix timestamp.
        * *updated* (``int``) --
          Unix timestamp.

    """

    def __init__(self, **kwargs):
        self._data = dict()
        self._refs = dict()
        self._dirty = False
        self._data_set('created', kwargs.get('created', int(time.time())))
        self._data_set('updated', kwargs.get('updated', self.created))
        if 'id' in kwargs:
            obj_id = kwargs.get('id')
        else:
            obj_id = generate_data_id()
        self._data_set('id', obj_id)
        _id_set.add(obj_id)
        _register_instance(self)

    def __getstate__(self):
        state = self.__dict__.copy()
        del state['_refs']
        del state['_dirty']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        self._dirty = False
        self._refs = dict()
        _id_set.add(self._data_get('id'))
        _register_instance(self)

    def __del__(self):
        _id_set.remove(self._data_get('id'))
        _unregister_instance(self)

    def _data_get(self, key, default=None):
        if key in self._data:
            return self._data.get(key)
        return default

    def _data_get_str(self, key) -> str | None:
        v = self._data_get(key, None)
        if v:
            return str(v)
        else:
            return None

    def _data_get_int(self, key) -> int | None:
        v = self._data_get(key, None)
        if v is not None and v != '':
            return int(v)
        else:
            return None

    def _data_get_bool(self, key) -> bool | None:
        v = self._data_get(key, None)
        if v is not None and v != '':
            return bool(v)
        else:
            return None

    def _data_set(self, key, value):
        if key in self._data and self._data[key] == value:
            return
        self._data[key] = value
        self._data['updated'] = int(time.time())
        self._dirty = True

    @property
    def id(self) -> str:
        return self._data_get_str('id')

    @property
    def created(self) -> datetime:
        return datetime.fromtimestamp(self._data_get('created'))

    @property
    def updated(self) -> datetime:
        return datetime.fromtimestamp(self._data_get('updated'))

    def output(self):
        out = ''
        data = copy.copy(self._data)
        out += "ID: %s\n" % data['id']
        out += "Type: %s\n" % self.__class__.__name__
        created = datetime.fromtimestamp(data['created'])
        updated = datetime.fromtimestamp(data['updated'])
        out += "Created: %s\n" % created.strftime('%x %X')
        out += "Updated: %s\n" % updated.strftime('%x %X')
        del data['id']
        del data['created']
        del data['updated']
        for k in sorted(data.keys()):
            out += "%s: %s\n" % (k, data[k])
        return out

    @property
    def needs_save(self):
        return self._dirty

    def to_backup(self):
        return {
            't': self.__class__.__name__,
            'd': copy.copy(self._data)
        }

    def from_backup(self, backup: dict):
        if backup['t'] != self.__class__.__name__:
            raise Exception("Invalid backup object type %s" % backup['t'])
        self._data = copy.copy(backup['d'])


_instances: dict[str, set[OehbdbObject]] = {}


def _register_instance(obj: OehbdbObject):
    obj_type = obj.__class__
    if obj_type.__name__ not in _instances:
        _instances[obj_type.__name__] = set()
    _instances[obj_type.__name__].add(obj)


def _unregister_instance(obj: OehbdbObject):
    obj_type = obj.__class__
    if obj_type.__name__ in _instances:
        _instances[obj_type.__name__].remove(obj)


def dump_instances():
    """
    Debug function
    :return:
    """
    for obj_type in _instances:
        print("[%s] ===========" % obj_type)
        for obj in _instances[obj_type]:
            print("[%s] %s (%s)" % (obj_type, obj.name, obj.id))


def object_by_id(obj_type: type, obj_id: str):
    for obj in _instances[obj_type.__name__]:
        if obj.id == obj_id:
            return obj


def objects(obj_type: type | None = None) -> set[OehbdbObject]:
    if obj_type:
        if obj_type.__name__ in _instances:
            return _instances[obj_type.__name__]
        else:
            return set()
    all_objects = set()
    for t in object_types():
        if t.__name__ in _instances:
            all_objects.update(_instances[t.__name__])
    return all_objects


class NuligaObject(OehbdbObject):
    """
    :param **kwargs:
        See below

    :Keyword Arguments:
        * *nuliga_data* (``dict[str, any]``) --
          nuliga information read from scrapliga feeds.
        * *name* (``str``) --
          Entity name as found on https://oehb-handball.liga.nu/.

    """

    def __init__(self, **kwargs):
        self._nuliga_data = dict()
        if 'nuliga_data' in kwargs:
            nuliga_data = kwargs.get('nuliga_data')
            for k, v in nuliga_data.items():
                if v is None:
                    continue
                self._nuliga_data[k] = v
            # inject 'created' timestamp to OehbdbObject if not set explicitly anyway
            if 'created' not in kwargs and 'nuliga_timestamp' in nuliga_data:
                kwargs['created'] = nuliga_data['nuliga_timestamp']
        OehbdbObject.__init__(self, **kwargs)
        if 'name' in kwargs:
            self._data_set('name', kwargs['name'])

    def nuliga_data(self, key):
        if key in self._nuliga_data:
            return self._nuliga_data[key]

    def nuliga_data_set(self, key, value):
        if value is None and key in self._nuliga_data:
            del self._nuliga_data[key]
        self._nuliga_data[key] = value
        # set 'updated' timestamp if nuliga_timestamp is more current and value gets actually served form nuliga_data
        if self._nuliga_data['nuliga_timestamp'] > self._data_get('updated') and self._data_get(key) == value:
            self._data_set('updated', self._nuliga_data['nuliga_timestamp'])

    def _data_get(self, key, default=None):
        # check for nuliga_data if key is not set in data dict
        if key in self._data:
            return self._data.get(key)
        if key in self._nuliga_data:
            return self._nuliga_data.get(key)
        return default

    def _ref_nuliga_backed(self, obj_type: type, prop_name: str):
        if prop_name not in self._refs:
            ref_obj_nuliga_name = self.nuliga_data(prop_name)
            self._refs[prop_name] = object_by_name(obj_type, ref_obj_nuliga_name)
            if not self._refs[prop_name]:
                logger.warning("No %s object found for nuliga property %s: \"%s\"" % (obj_type.__name__, prop_name,
                                                                                      ref_obj_nuliga_name))
        return self._refs[prop_name]


    @property
    def nuliga_id(self) -> str | None:
        return self.nuliga_data('nuliga_id')

    @property
    def name(self) -> str | None:
        return self._data_get_str('name')

    def output(self):
        out = OehbdbObject.output(self)
        nu_data = copy.copy(self._nuliga_data)
        ts = datetime.fromtimestamp(nu_data['nuliga_timestamp'])
        if 'nuliga_id' in nu_data:
            out += "nuliga ID: %s\n" % nu_data['nuliga_id']
            del nu_data['nuliga_id']
        else:
            out += "nuliga ID: <none>\n"
        out += "nuliga timestamp: %s\n" % ts
        del nu_data['nuliga_timestamp']
        for k in sorted(nu_data.keys()):
            out += "[nuliga] %s: %s\n" % (k, nu_data[k])
        return out

    def to_backup(self):
        d = OehbdbObject.to_backup(self)
        d['nu'] = self._nuliga_data
        return d

    def from_backup(self, backup: dict):
        OehbdbObject.from_backup(self, backup)
        self._nuliga_data = copy.copy(backup['nu'])


def object_by_name(obj_type: type, obj_name: str):
    if obj_type.__name__ not in _instances:
        return None
    for obj in _instances[obj_type.__name__]:
        if isinstance(obj, NuligaObject) and obj.name == obj_name:
            return obj


class Championship(NuligaObject):
    """
    :param **kwargs:
        See below

    """

    def __init__(self, **kwargs):
        NuligaObject.__init__(self, **kwargs)

    @property
    def short_name(self):
        return self._data_get_str('short_name')

    def groups(self):
        groups = set()
        for group in objects(Group):  # type: Group
            if group.championship == self:
                groups.add(group)
        return groups

    def clubs(self):
        clubs = set()
        for group in self.groups():
            clubs.update(group.clubs())
        return clubs

    def matches(self):
        matches = set()
        for match in objects(Match):  # type: Match
            if match.championship() == self:
                matches.add(match)
        return matches


class Club(NuligaObject):
    """
    :param **kwargs:
        See below

    """

    def __init__(self, **kwargs):
        NuligaObject.__init__(self, **kwargs)

    @property
    def website(self) -> str | None:
        return self._data_get_str('website')

    def teams(self) -> set:
        teams = set()
        for team in objects(Team):  # type: Team
            if team.club == self:
                teams.add(team)
        return teams

    def groups(self) -> set:
        groups = set()
        for group in objects(Group):  # type: Group
            if self in group.clubs():
                groups.add(group)
        return groups


class Group(NuligaObject):
    """
    :param **kwargs:
        See below

    """

    def __init__(self, **kwargs):
        NuligaObject.__init__(self, **kwargs)
        self._standings = None

    def __getstate__(self):
        state = NuligaObject.__getstate__(self)
        del state['_standings']
        return state

    def __setstate__(self, state):
        NuligaObject.__setstate__(self, state)
        self._standings = None

    @property
    def championship(self) -> Championship | None:
        if 'championship' not in self._refs:
            championship_short_name = self.nuliga_data('championship')
            for championship in objects(Championship):  # type: Championship
                if championship.short_name == championship_short_name:
                    self._refs['championship'] = championship
        return self._refs['championship']

    def teams(self) -> set[Team]:
        teams = set()
        for team in objects(Team):  # type: Team
            if team.group == self:
                teams.add(team)
        return teams

    def clubs(self) -> set[Club]:
        clubs = set()
        for team in self.teams():
            if team.club:
                clubs.add(team.club)
        return clubs

    def matches(self) -> set[Match]:
        matches = set()
        for match in objects(Match):  # type: Match
            if match.group() == self:
                matches.add(match)
        return matches

    def standings(self) -> list[GroupStanding]:
        if not self._standings:
            self._standings = list()
            standings = self.nuliga_data('standings')
            if not standings:
                return self._standings
            for item in standings:
                for team in self.teams():
                    if team.match_display_name != item['team']:
                        continue
                    if team.noncompetitive:
                        s = GroupStanding(self, item['rank'], team)
                    else:
                        s = GroupStandingCompetitive(self, item['rank'], team, item['played'], item['won'],
                                                     item['drawn'], item['lost'],  item['goal_for'],
                                                     item['goal_against'], item['points'])
                    self._standings.append(s)
                    break
            self._standings.sort(key=lambda standing: standing.rank)
        return self._standings


class GroupStanding:

    def __init__(self, group: Group, rank: int, team: Team):
        self._group = group
        self._rank = rank
        self._team = team

    @property
    def group(self) -> Group:
        return self._group

    @property
    def rank(self) -> int:
        return self._rank

    @property
    def team(self) -> Team:
        return self._team

    @property
    def competitive(self) -> bool:
        return False


class GroupStandingCompetitive(GroupStanding):

    def __init__(self, group: Group, rank: int, team: Team, played: int, won: int, drawn: int, lost: int,
                 goals_for: int,  goals_against: int, points: int):
        GroupStanding.__init__(self, group, rank, team)
        self._played = played
        self._won = won
        self._drawn = drawn
        self._lost = lost
        self._goals_for = goals_for
        self._goals_against = goals_against
        self._points = points

    @property
    def competitive(self) -> bool:
        return True

    @property
    def played(self) -> int:
        return self._played

    @property
    def won(self) -> int:
        return self._won

    @property
    def drawn(self) -> int:
        return self._drawn

    @property
    def lost(self) -> int:
        return self._lost

    @property
    def goals_for(self) -> int:
        return self._goals_for

    @property
    def goals_against(self) -> int:
        return self._goals_against

    @property
    def goals_diff(self) -> int:
        return self._goals_for - self._goals_against

    @property
    def points(self) -> int:
        return self._points


class Court(NuligaObject):
    """
    :param **kwargs:
        See below

    """

    def __init__(self, **kwargs):
        NuligaObject.__init__(self, **kwargs)

    @property
    def address(self) -> str | None:
        return self._data_get_str('address')

    @property
    def postal_code(self) -> str | None:
        return self._data_get_str('postal_code')

    @property
    def city(self) -> str | None:
        return self._data_get_str('city')


class Team(NuligaObject):
    """
    :param **kwargs:
        See below

    """

    def __init__(self, **kwargs):
        NuligaObject.__init__(self, **kwargs)

    @property
    def match_display_name(self) -> str | None:
        return self._data_get_str('match_display_name')

    @property
    def noncompetitive(self) -> str | None:
        return self._data_get_bool('noncompetitive')

    @property
    def club(self) -> Club | None:
        return self._ref_nuliga_backed(Club, 'club')

    @property
    def group(self) -> Group | None:
        return self._ref_nuliga_backed(Group, 'group')

    @property
    def championship(self) -> Championship | None:
        if 'championship' not in self._refs:
            championship_short_name = self.nuliga_data('championship')
            for championship in objects(Championship):  # type: Championship
                if championship.short_name == championship_short_name:
                    self._refs['championship'] = championship
        return self._refs['championship']


class Match(NuligaObject):

    def __init__(self, **kwargs):
        NuligaObject.__init__(self, **kwargs)

    def championship(self):
        return self._ref_nuliga_backed(Championship, 'championship')

    def group(self) -> Group | None:
        return self._ref_nuliga_backed(Group, 'group')

    @property
    def number(self) -> int | None:
        return self._data_get_int('number')

    def __get_team_ref(self, team_a_or_b) -> Team | None:
        if team_a_or_b not in self._refs:
            team_name = self.nuliga_data(team_a_or_b)
            group = self.group()
            if group:
                for team in group.teams():
                    if team.match_display_name == team_name:
                        self._refs[team_a_or_b] = team
                        break
            if team_a_or_b not in self._refs or not self._refs[team_a_or_b]:
                group_name = group.name if group else "??no-group-name??"
                logger.warning("No Team object found for nuliga property %s in group %s: \"%s\"" %
                               (team_a_or_b, group_name, team_name))
        if team_a_or_b in self._refs:
            return self._refs[team_a_or_b]
        else:
            return None

    def team_a(self) -> Team | None:
        return self.__get_team_ref('team_a')

    def team_b(self) -> Team | None:
        return self.__get_team_ref('team_b')

    @property
    def date(self) -> datetime | None:
        if 'date' in self._data:
            return datetime.fromtimestamp(self._data['date'])
        elif 'date' in self._nuliga_data:
            nuliga_date = self._nuliga_data.get('date')
            return datetime.strptime("%s UTC" % nuliga_date, "%Y-%m-%d %H:%M:%S %Z")
        return None

    def court(self) -> Court | None:
        return self._ref_nuliga_backed(Court, 'court')

    def noncompetitive(self) -> bool:
        return (self.team_a().noncompetitive or True) and (self.team_b().noncompetitive or True)

    def gloals_a(self) -> int | None:
        return self._data_get_int('gloals_a')

    def gloals_b(self) -> int | None:
        return self._data_get_int('gloals_b')


def object_types():
    return [Championship, Group, Club, Team, Court, Match]

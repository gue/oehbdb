import atexit
import copy
import logging
import shelve
from oehbdb import data

logger = logging.getLogger()

_shelf_file: str | None = None
_shelf: shelve.Shelf | None = None
_shelf_dirty: bool = False
_obj_id_dict: dict[str, data.OehbdbObject] | None = None
_club_nuliga_id_dict: dict[str, data.Club] | None = None
_group_nuliga_id_dict: dict[str, data.Group] | None = None
_court_nuliga_id_dict: dict[str, data.Court] | None = None
_team_nuliga_id_dict: dict[str, data.Team] | None = None


def _cache_warmup():
    global _obj_id_dict, _club_nuliga_id_dict, _group_nuliga_id_dict
    global _court_nuliga_id_dict, _team_nuliga_id_dict
    _obj_id_dict = dict()
    _club_nuliga_id_dict = dict()
    _group_nuliga_id_dict = dict()
    _court_nuliga_id_dict = dict()
    _team_nuliga_id_dict = dict()
    for club in _shelf['clubs']:
        _cache_add_object(club)
    for group in _shelf['groups']:
        _cache_add_object(group)
    for court in _shelf['courts']:
        _cache_add_object(court)
    for team in _shelf['teams']:
        _cache_add_object(team)
    for championship in _shelf['championships']:
        _cache_add_object(championship)
    for match in _shelf['matches']:
        _cache_add_object(match)


def _cache_add_object(obj: data.OehbdbObject):
    global _obj_id_dict
    _obj_id_dict[obj.id] = obj
    if isinstance(obj, data.Group):
        global _group_nuliga_id_dict
        _group_nuliga_id_dict[obj.nuliga_id] = obj
    elif isinstance(obj, data.Club):
        global _club_nuliga_id_dict
        _club_nuliga_id_dict[obj.nuliga_id] = obj
    elif isinstance(obj, data.Court):
        global _court_nuliga_id_dict
        _court_nuliga_id_dict[obj.nuliga_id] = obj
    elif isinstance(obj, data.Team):
        global _team_nuliga_id_dict
        _team_nuliga_id_dict[obj.nuliga_id] = obj


def _cache_clear():
    global _obj_id_dict, _club_nuliga_id_dict, _group_nuliga_id_dict
    global _court_nuliga_id_dict, _team_nuliga_id_dict
    _obj_id_dict = None
    _club_nuliga_id_dict = None
    _group_nuliga_id_dict = None
    _court_nuliga_id_dict = None
    _team_nuliga_id_dict = None


def _cache_evict_obj(obj: data.OehbdbObject):
    del _obj_id_dict[obj.id]
    if isinstance(obj, data.Group):
        del _group_nuliga_id_dict[obj.nuliga_id]
    elif isinstance(obj, data.Club):
        del _club_nuliga_id_dict[obj.nuliga_id]
    elif isinstance(obj, data.Court):
        del _court_nuliga_id_dict[obj.nuliga_id]
    elif isinstance(obj, data.Team):
        del _team_nuliga_id_dict[obj.nuliga_id]


def open(file: str):
    global _shelf, _shelf_file, _shelf_dirty
    if _shelf:
        raise RuntimeError("already open")

    _shelf = shelve.open(file, writeback=True)
    if "championships" not in _shelf:
        _shelf["championships"] = set()
    if "groups" not in _shelf:
        _shelf["groups"] = set()
    if "clubs" not in _shelf:
        _shelf["clubs"] = set()
    if "courts" not in _shelf:
        _shelf["courts"] = set()
    if "teams" not in _shelf:
        _shelf["teams"] = set()
    if "matches" not in _shelf:
        _shelf["matches"] = set()
    _cache_warmup()
    _shelf_file = file
    _shelf_dirty = False
    atexit.register(_exit)
    logger.debug("Database %s opened" % file)


def save():
    _shelf.sync()


def close():
    _exit()
    atexit.unregister(_exit)


def _exit():
    global _shelf, _shelf_file, _shelf_dirty
    if _shelf:
        dirty = _shelf_dirty
        if not _shelf_dirty:
            for obj in data.objects():
                if obj.needs_save:
                    dirty = True
                    break
        if dirty:
            _shelf.close()
            logger.debug("Database %s written" % _shelf_file)
    _shelf = None
    _cache_clear()
    _shelf_file = None
    _shelf_dirty = False


def add_data(obj: data.OehbdbObject):
    global _shelf_dirty
    if obj.id in _obj_id_dict.keys():
        return obj
    if isinstance(obj, data.Championship):
        _shelf['championships'].add(obj)
        _cache_add_object(obj)
        logger.info("Add championship %s (%s)" % (obj.name, obj.id))
    elif isinstance(obj, data.Club):
        _shelf['clubs'].add(obj)
        _cache_add_object(obj)
        logger.info("Add club %s (%s)" % (obj.name, obj.id))
    elif isinstance(obj, data.Group):
        _shelf['groups'].add(obj)
        _cache_add_object(obj)
        champ_name = obj.championship.name if obj.championship else "??no-championship??"
        logger.info("Add group %s: %s (%s)" % (champ_name, obj.name, obj.id))
    elif isinstance(obj, data.Court):
        _shelf['courts'].add(obj)
        _cache_add_object(obj)
        logger.info("Add court %s (%s)" % (obj.name, obj.id))
    elif isinstance(obj, data.Team):
        _shelf['teams'].add(obj)
        _cache_add_object(obj)
        group_name = obj.group.name if obj.group else "??no-group??"
        logger.info("Add team %s - %s: %s (%s)" % (group_name, obj.club.name, obj.name, obj.id))
    elif isinstance(obj, data.Match):
        _shelf['matches'].add(obj)
        team_a = obj.team_a()
        a_name = team_a.match_display_name if team_a else "??"
        team_b = obj.team_b()
        b_name = team_b.match_display_name if team_b else "??"
        logger.info("Add match %s: #%d %s vs. %s (%s)" % (obj.group().name, obj.number, a_name, b_name, obj.id))
    else:
        raise Exception("Unsupported object type %s" % obj.__class__.__name__)
    _shelf_dirty = True
    return obj


def del_data(obj: data.OehbdbObject) -> bool:
    global _shelf_dirty
    if obj.id not in _obj_id_dict.keys():
        return False
    _cache_evict_obj(obj)
    if isinstance(obj, data.Championship):
        logger.info("Delete championship %s (%s)" % (obj.name, obj.id))
        _shelf['championships'].remove(obj)
    elif isinstance(obj, data.Group):
        championship_name = obj.championship.name if obj.championship else "??no-championship??"
        logger.info("Delete group %s: %s (%s)" % (championship_name, obj.name, obj.id))
        _shelf['groups'].remove(obj)
    elif isinstance(obj, data.Court):
        logger.info("Delete court %s (%s)" % (obj.name, obj.id))
        _shelf['courts'].remove(obj)
    elif isinstance(obj, data.Club):
        logger.info("Delete club %s (%s)" % (obj.name, obj.id))
        _shelf['clubs'].remove(obj)
    elif isinstance(obj, data.Team):
        group_name = obj.group.name if obj.group else "??no-group??"
        club_name = obj.club.name if obj.club else "??no-club??"
        logger.info("Delete team %s - %s: %s (%s)" % (group_name, club_name, obj.name, obj.id))
        _shelf['teams'].remove(obj)
    elif isinstance(obj, data.Match):
        team_a = obj.team_a()
        a_name = team_a.match_display_name if team_a else "??"
        team_b = obj.team_b()
        b_name = team_b.match_display_name if team_b else "??"
        group_name = obj.group().name if obj.group() else "??no-group??"
        obj_nr = "#%d" % obj.number if obj.number else "??no-number??"
        logger.info("Delete match %s: %s %s vs. %s (%s)" % (group_name, obj_nr, a_name, b_name, obj.id))
        _shelf['matches'].remove(obj)
    else:
        raise Exception("Unsupported object type %s" % obj.__class__.__name__)
    _shelf_dirty = True
    return True


def purge():
    global _shelf_dirty
    _shelf['championships'].clear()
    _shelf['groups'].clear()
    _shelf['courts'].clear()
    _shelf['clubs'].clear()
    _shelf['teams'].clear()
    _shelf['matches'].clear()
    _shelf_dirty = True
    _cache_clear()
    _cache_warmup()


def get_all() -> set[data.OehbdbObject]:
    objects = set()
    for obj in _shelf['championships']:
        objects.add(obj)
    for obj in _shelf['groups']:
        objects.add(obj)
    for obj in _shelf['courts']:
        objects.add(obj)
    for obj in _shelf['clubs']:
        objects.add(obj)
    for obj in _shelf['teams']:
        objects.add(obj)
    for obj in _shelf['matches']:
        objects.add(obj)
    return objects


def get_obj_by_id(obj_id: str) -> data.OehbdbObject | None:
    return _obj_id_dict.get(obj_id, None)


def get_championship_by_nuliga_id(nuliga_id: str) -> data.Club | None:
    for championship in _shelf['championships']:
        if championship.nuliga_id == nuliga_id:
            return championship


def get_club_by_nuliga_id(nuliga_id: str) -> data.Club | None:
    return _club_nuliga_id_dict.get(nuliga_id, None)


def get_group_by_nuliga_id(nuliga_id: str) -> data.Group | None:
    return _group_nuliga_id_dict.get(nuliga_id, None)


def get_court_by_nuliga_id(nuliga_id: str) -> data.Court | None:
    return _court_nuliga_id_dict.get(nuliga_id, None)


def get_team_by_nuliga_id(nuliga_id: str) -> data.Team | None:
    return _team_nuliga_id_dict.get(nuliga_id, None)


def get_match_by_nuliga_id(nuliga_id: str) -> data.Match | None:
    for match in _shelf['matches']:
        if match.nuliga_data('nuliga_id') == nuliga_id:
            return match


def get_match_by_nuliga_item(match_item: dict) -> data.Match | None:
    # this lookup function is really messy, but nuliga data is not very helpful here...
    # Example: https://oehb-handball.liga.nu/cgi-bin/WebObjects/nuLigaHBAT.woa/wa/groupPage?displayTyp=vorrunde&displayDetail=meetings&championship=StHV+22%2F23&group=211800
    for match in _shelf['matches']:
        if match.nuliga_data('championship') == match_item['championship'] \
                and match.nuliga_data('group') == match_item['group']\
                and match.nuliga_data('team_a') == match_item['team_a']\
                and match.nuliga_data('team_b') == match_item['team_b']\
                and match.nuliga_data('number') == match_item['match_number']:
            return match


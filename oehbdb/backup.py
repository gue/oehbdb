import json
import logging

from oehbdb import store, data

logger = logging.getLogger()


def backup(file: str):
    objects = store.get_all()
    objects = list(objects)
    objects.sort(key=lambda x: x.id)
    exports = list()
    for obj in objects:
        exports.append(json.dumps(obj.to_backup()))
    with open(file, 'w') as f:
        f.write("oehbdb backup file. See: https://codeberg.org/gue/oehbdb")
        for dumped_object in exports:
            f.write("%s\n" % dumped_object)
    logger.info("Backup of %d objects written to %s" % (len(exports), file))


def restore(file: str):
    backup_file = open(file, 'r')
    logger.info("Reading backup file %s" % file)
    restored_objects = set()
    first_line = backup_file.readline()
    if not first_line.startswith('oehbdb backup file.'):
        logger.error("Invalid backup")
        return
    for dumped_obj in backup_file:
        if not dumped_obj.startswith('{'):
            continue
        backup_obj = json.loads(dumped_obj)
        obj = _create_obj(backup_obj['t'])
        obj.from_backup(backup_obj)
        restored_objects.add(obj)

    store.purge()
    for obj in restored_objects:
        store.add_data(obj)
    logger.info("Restored %d objects" % len(restored_objects))


def _create_obj(obj_type: str) -> data.OehbdbObject:
    type_map = {
        data.Championship.__name__: data.Championship,
        data.Group.__name__: data.Group,
        data.Court.__name__: data.Court,
        data.Club.__name__: data.Club,
        data.Team.__name__: data.Team,
    }
    return type_map.get(obj_type)()
